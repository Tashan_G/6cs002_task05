package Football;



/******************************************************************************
 * This class stores the details of a Premiership Football team including the 
 * performance measures that determine their placement in the league. The class 
 * implements the Comparable interface, which determines how clubs will be 
 * sorted.     
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Gaelic_FootballClub implements Comparable<Gaelic_FootballClub> {
  private int placement;
  private String team;
  private int matchesPlayed;
  private int matchesWon;
  private int matchesDrawn;
  private int matchesLost;
  private int goalsFor;
  private int goalsAgainst;
  private int goalsDifference;
  private int triesFor;
  private int triesAgainst;
  private int tryBonusPoints;
  private int losingBonusPoints;
  private int totalPoints;

  public Gaelic_FootballClub(int placement, String team, int matchesPlayed, int matchesWon, int matchesDrawn,
      int matchesLost, int goalsFor, int goalsAgainst, int goalsDifference,
      int triesFor, int triesAgainst, int tryBonusPoints, int losingBonusPoints,
      int totalPoints) {
    this.placement = placement;
    this.team = team;
    this.matchesPlayed = matchesPlayed;
    this.matchesWon = matchesWon;
    this.matchesDrawn = matchesDrawn;
    this.matchesLost = matchesLost;
    this.goalsFor = goalsFor;
    this.goalsAgainst = goalsAgainst;
    this.goalsDifference = goalsDifference;
    this.triesFor = triesFor;
    this.triesAgainst = triesAgainst;
    this.tryBonusPoints = tryBonusPoints;
    this.losingBonusPoints = losingBonusPoints;
    this.totalPoints = totalPoints;
  }

  public String toString() {
    return String.format("%-3d%-20s%9d%12d%13d%13d", placement, team, totalPoints,
        matchesWon, matchesLost, matchesDrawn);
  }

  public int getplacement() {
    return placement;
  }

  public void setplacement(int placement) {
    this.placement = placement;
  }

  public String getteam() {
    return team;
  }

  public void setteam(String team) {
    this.team = team;
  }

  public int getmatchesPlayed() {
    return matchesPlayed;
  }

  public void setmatchesPlayed(int matchesPlayed) {
    this.matchesPlayed = matchesPlayed;
  }

  public int getmatchesWon() {
    return matchesWon;
  }

  public void setmatchWon(int matchWon) {
    this.matchesWon = matchWon;
  }

  public int getmatchesDrawn() {
    return matchesDrawn;
  }

  public void setmatchesDrawn(int matchesDrawn) {
    this.matchesDrawn = matchesDrawn;
  }

  public int getmatchesLost() {
    return matchesLost;
  }

  public void setmatchesLost(int matchesLost) {
    this.matchesLost = matchesLost;
  }

  public int getgoalsFor() {
    return goalsFor;
  }

  public void setgoalsFor(int goalsFor) {
    this.goalsFor = goalsFor;
  }

  public int getgoalsAgainst() {
    return goalsAgainst;
  }

  public void setgoalsAgainst(int goalsAgainst) {
    this.goalsAgainst = goalsAgainst;
  }

  public int getgoalsDifference() {
    return goalsDifference;
  }

  public void setgoalsDifference(int goalsDifference) {
    this.goalsDifference = goalsDifference;
  }

  public int gettriesFor() {
    return triesFor;
  }

  public void settriesFor(int triesFor) {
    this.triesFor = triesFor;
  }

  public int gettriesAgainst() {
    return triesAgainst;
  }

  public void settriesAgainst(int triesAgainst) {
    this.triesAgainst = triesAgainst;
  }

  public int gettryBonusPoints() {
    return tryBonusPoints;
  }

  public void settryBonusPoints(int tryBonusPoints) {
    this.tryBonusPoints = tryBonusPoints;
  }

  public int getlosingBonusPoints() {
    return losingBonusPoints;
  }

  public void setlosingBonusPoints(int losingBonusPoints) {
    this.losingBonusPoints = losingBonusPoints;
  }

  public int gettotalPoints() {
    return totalPoints;
  }

  public void settotalPoints(int totalPoints) {
    this.totalPoints = totalPoints;
  }
  
  public int compareTo(Gaelic_FootballClub c) {
    return ((Integer) goalsFor).compareTo(c.goalsFor);
  }
}
