package Football;


import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class Gaelic_Football03 {
  public static void main(String[] args) {
    List<Gaelic_FootballClub> table = Arrays.asList(
        new Gaelic_FootballClub(1, "New England Patriots", 22, 16, 1, 5, 621, 400, 221, 75, 41,
            8, 2, 76),
        new Gaelic_FootballClub(2, "Dallas Cowboys", 22, 16, 0, 6, 625, 414, 211, 72, 43, 9, 2, 75),
        new Gaelic_FootballClub(3, "Green Bay Packers", 22, 15, 1, 6, 453, 421, 32, 37, 39, 4,
            2, 68),
        new Gaelic_FootballClub(4, "Pittsburgh Steelers", 22, 14, 1, 7, 664, 418, 246, 70, 40, 5, 5, 68),
        new Gaelic_FootballClub(5, "San Francisco 49ers", 22, 14, 0, 8, 663, 437, 226, 70, 46, 5, 7,
            68),
        new Gaelic_FootballClub(6, "Kansas City Chiefs", 22, 11, 2, 9, 672, 527, 145, 77, 54, 9, 4, 61),
        new Gaelic_FootballClub(7, "New York Giants", 22, 11, 0, 11, 497, 482, 15, 62, 54, 6, 4,
            54),
        new Gaelic_FootballClub(8, "Chicago Bears", 22, 10, 0, 12, 444, 514, -70, 45, 50, 4, 5,
            49),
        new Gaelic_FootballClub(9, "Philadelphia Eagles", 22, 9, 1, 12, 553, 575, -22, 53, 61, 4, 6,
            48),
        new Gaelic_FootballClub(10, "Los Angeles Rams", 22, 7, 1, 14, 442, 578, -136, 46, 57, 4, 6,
            40),
        new Gaelic_FootballClub(11, "Baltimore Ravens", 22, 5, 1, 16, 475, 545, -70, 57, 61,
            4, 8, 34),
        new Gaelic_FootballClub(12, "Seattle Seahawks", 22, 0, 0, 22, 223, 1021, -798, 29, 147, 1,
            0, 1));


    OptionalInt fb_min = table.stream().mapToInt(Gaelic_FootballClub::getgoalsFor).min();
    if (fb_min.isPresent()) {
      System.out.printf("Lowest Number Of Goals Scored By A Team : %d\n", fb_min.getAsInt());
    } else {
      System.out.println("Min Failed");
    }
    
    OptionalInt fb_max = table.stream().mapToInt(Gaelic_FootballClub::getgoalsFor).max();
    if (fb_max.isPresent()) {
    	System.out.printf("Highest Number Of Goals Scored By A Team : %d\n", fb_max.getAsInt());
    } else {
    	System.out.println("Max Failed");
    }
    
    // reduce
    System.out.println("Total Matches Won:");
    Integer fb_output = table.stream().map(Gaelic_FootballClub::getmatchesWon).reduce(0, (a, b) -> a + b);
    System.out.println(fb_output);
    
    //Collectors
    List<String> fb_result = table.stream().filter(p -> p.gettryBonusPoints() > 8).map(Gaelic_FootballClub::getteam)
    		.collect(Collectors.toList());
    System.out.println(fb_result.toString());
  
    try {
        FileWriter writer = new FileWriter("FB_OutPut03.txt");
        writer.write("Lowest Number Of Goals Scored By A Team : " + fb_min.getAsInt() + "\n");
        writer.write("Highest Number Of Goals Scored By A Team: " + fb_max + "\n");
        writer.write("Total Matches On: " + fb_output + "\n");
        writer.write("Teams Who Put Bonus Points: " + fb_result + "\n");
        writer.close();
        System.out.println("\nFB_OutPut03.txt file successfully written.");
      } catch (IOException e) {
        System.out.println("An error has occurred.");
        e.printStackTrace();
      }
  }

 
  }
