package Football;


import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/******************************************************************************
 * This example demonstrates processing a list of integers with a stream.     
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/


public class Gaelic_Football01 {
  public static void main(String[] args) {
    List<Gaelic_FootballClub> table = Arrays.asList(
        new Gaelic_FootballClub(1, "New England Patriots", 22, 16, 1, 5, 621, 400, 221, 75, 41,
            8, 2, 76),
        new Gaelic_FootballClub(2, "Dallas Cowboys", 22, 16, 0, 6, 625, 414, 211, 72, 43, 9, 2, 75),
        new Gaelic_FootballClub(3, "Green Bay Packers", 22, 15, 1, 6, 453, 421, 32, 37, 39, 4,
            2, 68),
        new Gaelic_FootballClub(4, "Pittsburgh Steelers", 22, 14, 1, 7, 664, 418, 246, 70, 40, 5, 5, 68),
        new Gaelic_FootballClub(5, "San Francisco 49ers", 22, 14, 0, 8, 663, 437, 226, 70, 46, 5, 7,
            68),
        new Gaelic_FootballClub(6, "Kansas City Chiefs", 22, 11, 2, 9, 672, 527, 145, 77, 54, 9, 4, 61),
        new Gaelic_FootballClub(7, "New York Giants", 22, 11, 0, 11, 497, 482, 15, 62, 54, 6, 4,
            54),
        new Gaelic_FootballClub(8, "Chicago Bears", 22, 10, 0, 12, 444, 514, -70, 45, 50, 4, 5,
            49),
        new Gaelic_FootballClub(9, "Philadelphia Eagles", 22, 9, 1, 12, 553, 575, -22, 53, 61, 4, 6,
            48),
        new Gaelic_FootballClub(10, "Los Angeles Rams", 22, 7, 1, 14, 442, 578, -136, 46, 57, 4, 6,
            40),
        new Gaelic_FootballClub(11, "Baltimore Ravens", 22, 5, 1, 16, 475, 545, -70, 57, 61,
            4, 8, 34),
        new Gaelic_FootballClub(12, "Seattle Seahawks", 22, 0, 0, 22, 223, 1021, -798, 29, 147, 1,
            0, 1));

    System.out.println("   CLUB NAME                POINTS    MATCHES_WON  MATCHES_LOSS  MATCHES_DRAWN  \n");
    table.forEach(b -> System.out.println(b));
   
    try {
			FileWriter writer = new FileWriter("FB_OutPut01.txt");
			writer.write("   CLUB NAME                POINTS    MATCHES_WON  MATCHES_LOSS MATCHES_DRAWN  \n");
			writer.write("   ---------                ------     ---------    ----------      ------\n");
			table.forEach(x -> {
			try {
				writer.write(x + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
			writer.close();
			System.out.println("\nFB_OutPut01.txt file successfully written");
	    } catch (IOException e) {
	    	System.out.println("An error has occurred.");
	    	e.printStackTrace();
	    }

 }
 

}
