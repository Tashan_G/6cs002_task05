package Football;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Gaelic_Football_GUI extends JFrame {
private JPanel contentPane;
	
	public Gaelic_Football_GUI() {
		setResizable(false);
		setTitle("6CS002_Task05_2065004");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 600);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel title = new JLabel("<html><h1><strong><i>Gaelic_Football</i></strong></h1><hr></html>");
		title.setBounds(210, 34, 45, 16);
		title.resize(300, 50);
		contentPane.add(title);
		
		JButton basktbl01 = new JButton("Gaelic_Football 01");
		basktbl01.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Gaelic_Football01.main(null);
			}
		});
		basktbl01.setBounds(200, 80, 117, 29);
		basktbl01.resize(200, 50);
		contentPane.add(basktbl01);
		
		JButton basktbl02 = new JButton("Gaelic_Football 02");
		basktbl02.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Gaelic_Football02.main(null);
			}
		});
		basktbl02.setBounds(200, 150, 117, 29);
		basktbl02.resize(200, 50);
		contentPane.add(basktbl02);
		
		JButton basktbl03 = new JButton("Gaelic_Football 03");
		basktbl03.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Gaelic_Football03.main(null);
			}
		});
		basktbl03.setBounds(200, 220, 117, 29);
		basktbl03.resize(200, 50);
		contentPane.add(basktbl03);
		
		JButton basktbl04 = new JButton("Gaelic_Football 04");
		basktbl04.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Gaelic_Football04.main(null);
			}
		});
		basktbl04.setBounds(200, 290, 117, 29);
		basktbl04.resize(200, 50);
		contentPane.add(basktbl04);
		
		JButton basktbl05 = new JButton("Gaelic_Football 05");
		basktbl05.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Gaelic_Football05.main(null);
			}
		});
		basktbl05.setBounds(200, 360, 117, 29);
		basktbl05.resize(200, 50);
		contentPane.add(basktbl05);
		
		JButton exit = new JButton("exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit.setBounds(200, 440, 117, 29);
		exit.resize(200, 50);
		contentPane.add(exit);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gaelic_Football_GUI frame = new Gaelic_Football_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
